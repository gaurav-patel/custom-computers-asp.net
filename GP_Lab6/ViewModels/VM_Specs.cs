﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GP_Lab6.ViewModels
{
    public class IndexSpecs
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Product Name")]
        public string Name { get; set; }

        public double Price { get; set; }
    }

    public class SpecsFull
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(40, MinimumLength = 3, ErrorMessage = "Name must be minimum of 3 characters long.")]
        public string Name { get; set; }

        [Required]
        [StringLength(125, MinimumLength = 10, ErrorMessage = "Description must be minimum of 10 characters long and less then 125.")]
        public string Description { get; set; }

        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Quantity must be greater then 0.")]
        public int Quantity { get; set; }

        [Required]
        [Range(0.01, double.MaxValue, ErrorMessage = "Price cannot be negative value.")]
        public double Price { get; set; }
    }
}