﻿using GP_Lab6.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GP_Lab6.ViewModels
{
    public class Repo_Specs : DBConnection
    {
        public List<IndexSpecs> getListOfComputerSpecs()
        {
            var specs = dc.Specifications.OrderBy(n => n.Name);

            List<IndexSpecs> ls = new List<IndexSpecs>();

            foreach (var item in specs)
            {
                IndexSpecs spec = new IndexSpecs();
                spec.Id = item.ProductNumber;
                spec.Name = item.Name;
                spec.Price = item.Price;

                ls.Add(spec);
            }

            return ls;
        }

        public SpecsFull getSpecificationFull(int? id)
        {
            var AllSpecs = dc.Specifications.FirstOrDefault(n => n.ProductNumber == id);

            SpecsFull spec = new SpecsFull();

            if (AllSpecs != null)
            {
                spec.Id = AllSpecs.ProductNumber;
                spec.Name = AllSpecs.Name;
                spec.Description = AllSpecs.Description;
                spec.Quantity = AllSpecs.Quantity;
                spec.Price = AllSpecs.Price;

                return spec;
            }
            else
            {
                return null;
            }
        }

        public SpecsFull createSpecification(SpecsFull spec)
        {
            var Id = dc.Specifications.Max(n => n.ProductNumber) + 1;

            Specifications newSpec = new Specifications(Id, spec.Name, spec.Description, spec.Price, spec.Quantity);
            dc.Specifications.Add(newSpec);

            // HACK, will be changed later
            dc.SaveChanges();

            return getSpecificationFull(Id);
        }

        public SpecsFull editSpecification(int id, SpecsFull product)
        {
            dc.Specifications.Single(t => t.ProductNumber == id).Name = product.Name;
            dc.Specifications.Single(t => t.ProductNumber == id).Description = product.Description;
            dc.Specifications.Single(t => t.ProductNumber == id).Price = product.Price;
            dc.Specifications.Single(t => t.ProductNumber == id).Quantity = product.Quantity;
            List<Computer> ls = new List<Computer>();
            dc.Specifications.Single(t => t.ProductNumber == id).computer = ls;
            
            dc.SaveChanges();

            return getSpecificationFull(id);
        }

        public bool deleteSpecification(int? id)
        {
            if (id > 0)
            {
                Specifications spec = dc.Specifications.Find(id);

                dc.Specifications.Remove(spec);
                dc.SaveChanges();

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}