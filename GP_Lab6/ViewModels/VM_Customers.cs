﻿using GP_Lab6.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GP_Lab6.ViewModels
{
    public class IndexClientComputers
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Order By")]
        public string Name { get; set; }

        public double Total { get; set; }

        [Display(Name = "Ordered Date")]
        public DateTime day { get; set; }
    }

    public class ComputerFull
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Ordered Date")]
        public DateTime orderedDay { get; set; }

        public string Name { get; set; }

        [Display(Name = "Street Address")]
        public string StreetAddress { get; set; }

        [Display(Name="Postal Code")]
        public string PostalCode { get; set; }

        [Display(Name = "Grand Total")]
        public double Total { get; set; }

        [Display(Name = "Computer Specifications")]
        public List<Specifications> specs { get; set; }
    }

    public class GetClientComputer
    {
        public void Clear()
        {
            Name = StreetAddress = PostalCode = string.Empty;
        }

        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        [Display(Name = "Street Address")]
        public string StreetAddress { get; set; }

        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }

        [Display(Name = "Computer Specifications")]
        public List<SelectListItem> specs { get; set; }
    }

    public class PostComputer
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(40, MinimumLength = 3, ErrorMessage = "Name must be minimum of 3 characters long.")]
        public string Name { get; set; }

        [Required]
        [StringLength(40, MinimumLength = 10, ErrorMessage = "Address must be minimum of 10 characters long.")]
        [Display(Name = "Street Address")]
        public string StreetAddress { get; set; }

        [Required]
        [RegularExpression("^([A-Za-z][0-9]){3}$", ErrorMessage = "Required in format A1A1A1 or a1a1a1")]
        public string PostalCode { get; set; }

        [Display(Name = "Ordered Date")]
        public DateTime orderedDay { get; set; }

        [Display(Name = "Computer Specifications")]
        [Required(ErrorMessage = "Must select at least one computer specification.")]
        public ICollection<int> specs { get; set; }
    }
}