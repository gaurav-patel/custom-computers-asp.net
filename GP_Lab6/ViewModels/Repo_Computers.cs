﻿using GP_Lab6.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GP_Lab6.ViewModels
{
    public class Repo_Computers : DBConnection
    {
        public List<IndexClientComputers> getListOfCustomersComputers()
        {
            var customers = dc.Computers.Include("Specs").OrderBy(n => n.CustomerName);

            List<IndexClientComputers> ls = new List<IndexClientComputers>();

            foreach (var item in customers)
            {
                IndexClientComputers computer = new IndexClientComputers();
                computer.Id = item.Id;
                computer.Name = item.CustomerName;
                computer.Total = item.Specs.Sum(t => t.Price);
                computer.day = item.orderedDay;

                ls.Add(computer);
            }

            return ls;
        }

        public ComputerFull getComputerFull(int? id)
        {
            var comp = dc.Computers.Include("Specs").FirstOrDefault(n => n.Id == id);

            ComputerFull or = new ComputerFull();

            if (comp != null)
            {
                or.Id = comp.Id;
                or.Name = comp.CustomerName;
                or.orderedDay = comp.orderedDay;
                or.PostalCode = comp.PostalCode;
                or.StreetAddress = comp.StreetAddress;
                or.specs = comp.Specs;
                or.Total = comp.Specs.Sum(t => t.Price);
                return or;
            }
            else
            {
                return null;
            }
        }

        public List<SelectListItem> createSelectListItems()
        {
            List<SelectListItem> ret_list = new List<SelectListItem>();
            var AllSpecs = dc.Specifications.OrderBy(n => n.Name);

            foreach (var item in AllSpecs)
            {
                SelectListItem tmp = new SelectListItem() { Text = item.Name, Value = item.ProductNumber.ToString(), Selected = false };
                ret_list.Add(tmp);
            }
            return ret_list;
        }

        public ComputerFull createComputer(PostComputer newComp)
        {
            Computer comp = new Computer();

            comp.Id = dc.Computers.Max(n => n.Id) + 1;
            comp.CustomerName = newComp.Name;
            comp.PostalCode = newComp.PostalCode;
            comp.StreetAddress = newComp.StreetAddress;
            comp.orderedDay = DateTime.Now;

            comp.Specs = new List<Specifications>();

            foreach (var item in newComp.specs)
            {
                var b = dc.Specifications.Find(item);
                if (b != null) comp.Specs.Add(b);
            }

            dc.Computers.Add(comp);
            dc.SaveChanges();

            return getComputerFull(comp.Id);
        }

        public GetClientComputer getComputerforEdit(int? id)
        {
            Computer comp = dc.Computers.Include("Specs").FirstOrDefault(n => n.Id == id);
            GetClientComputer forEdit = new GetClientComputer();

            if(comp == null)
            {
                return null;
            }
            else
            {
                forEdit.Id = comp.Id;
                forEdit.Name = comp.CustomerName;
                forEdit.PostalCode = comp.PostalCode;
                forEdit.StreetAddress = comp.StreetAddress;

                List<Specifications> selected = comp.Specs;
                List<SelectListItem> ret_list = new List<SelectListItem>();
                var AllSpecs = dc.Specifications.OrderBy(n => n.Name).AsEnumerable();


                foreach (var item in AllSpecs)
                {
                    SelectListItem tmp = new SelectListItem() { Text = item.Name, Value = item.ProductNumber.ToString(), Selected = false };

                    foreach (var part in selected)
                    {
                        if (item.ProductNumber == part.ProductNumber) tmp.Selected = true;
                    }

                    ret_list.Add(tmp);
                }

                forEdit.specs = ret_list;
                return forEdit;
            }
        }

        public ComputerFull updateComputer(int id, PostComputer edited)
        {
            var comp = dc.Computers.Include("Specs").FirstOrDefault(m => m.Id == id);

            comp.Specs.Clear();
            dc.Entry(comp).CurrentValues.SetValues(edited);
            comp.orderedDay = DateTime.Now;

            foreach (var part in edited.specs)
            {
                var b = dc.Specifications.Find(part);
                if(b != null) comp.Specs.Add(b);
            }

            dc.SaveChanges();
            return getComputerFull(id);
        }

        public bool deleteCustomerComputer(int? id)
        {
            if (id > 0 && id != null)
            {
                Computer customer = dc.Computers.Find(id);

                dc.Computers.Remove(customer);
                dc.SaveChanges();

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}