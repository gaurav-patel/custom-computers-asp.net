﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GP_Lab6.Models
{
    public class Computer
    {
        public Computer()
        {
            Specs = new List<Specifications>();
        }

        public Computer(int i, string n, string a, string c)
        {
            Specs = new List<Specifications>();
            Id = i;
            CustomerName = n;
            StreetAddress = a;
            PostalCode = c;
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(40, MinimumLength = 3, ErrorMessage = "Name must be minimum of 3 characters long.")]
        public string CustomerName { get; set; }

        [Required]
        [StringLength(40, MinimumLength = 10, ErrorMessage = "Address must be minimum of 10 characters long.")]
        [Display(Name = "Street Address")]
        public string StreetAddress { get; set; }

        [Required]
        [RegularExpression("^([A-Za-z][0-9]){3}$", ErrorMessage = "Required in format A1A1A1 or a1a1a1")]
        public string PostalCode { get; set; }

        [Display(Name = "Ordered Date")]
        public DateTime orderedDay { get; set; }

        [Required(ErrorMessage = "Must choose specifications/hardware for his computer.")]
        [Display(Name = "Computer Specifications")]
        public List<Specifications> Specs { get; set; }
    }

    public class Specifications
    {
        public Specifications()
        {
            computer = new List<Computer>();
        }

        public Specifications(int p, string n, string d, double m, int q)
        {
            computer = new List<Computer>();
            ProductNumber = p;
            Name = n;
            Description = d;
            Price = m;
            Quantity = q;
        }

        [Key]
        public int ProductNumber { get; set; }

        [Required]
        [StringLength(40, MinimumLength = 3, ErrorMessage = "Name must be minimum of 3 characters long.")]
        public string Name { get; set; }

        [Required]
        [StringLength(125, MinimumLength = 10, ErrorMessage = "Description must be minimum of 10 characters long and less then 125.")]
        public string Description { get; set; }

        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Quantity must be greater then 0.")]
        public int Quantity { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Price cannot be negative value.")]
        public double Price { get; set; }

        public List<Computer> computer { get; set; }
    }
}