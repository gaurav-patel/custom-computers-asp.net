﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace GP_Lab6.Models
{
    public class Initiallizer : DropCreateDatabaseAlways<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext dc)
        {
            Initialize_Users(dc);
            Initialize_ComputerOrders(dc);
        }

        protected void Initialize_Users(ApplicationDbContext dc)
        {
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dc));
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(dc));

            string adminRole = "Administrator";
            string adminEmail = "admin@myseneca.ca";
            string password = "123456";

            //Create Role Admin if it does not exist
            if (!RoleManager.RoleExists(adminRole))
            {
                var roleresult = RoleManager.Create(new IdentityRole(adminRole));
            }

            //Create admin=Admin with password=123456
            var adminUser = new ApplicationUser();
            adminUser.UserName = adminEmail;
            adminUser.Email = adminEmail;
            var adminresult = UserManager.Create(adminUser, password);

            if (adminresult.Succeeded)
            {
                var result = UserManager.AddToRole(adminUser.Id, adminRole);
                UserManager.CreateIdentity(adminUser, DefaultAuthenticationTypes.ApplicationCookie);
                UserManager.AddClaim(adminUser.Id, new Claim(ClaimTypes.Email, adminUser.Email));
                UserManager.AddClaim(adminUser.Id, new Claim(ClaimTypes.Role, adminRole));
            }

            //Creating Member user.
            string memberRole = "Member";
            string memberEmail = "mark@myseneca.ca";

            //Create Role Member if it does not exist
            if (!RoleManager.RoleExists(memberRole))
            {
                var roleresult = RoleManager.Create(new IdentityRole(memberRole));
            }

            //Create user=mark with member role with password=123456
            var memberUser = new ApplicationUser();
            memberUser.UserName = memberEmail;
            memberUser.Email = memberEmail;
            var memberresult = UserManager.Create(memberUser, password);

            if (memberresult.Succeeded)
            {
                var result = UserManager.AddToRole(memberUser.Id, memberRole);
                UserManager.CreateIdentity(memberUser, DefaultAuthenticationTypes.ApplicationCookie);
                UserManager.AddClaim(memberUser.Id, new Claim(ClaimTypes.Email, memberUser.Email));
                UserManager.AddClaim(memberUser.Id, new Claim(ClaimTypes.Role, memberRole));
            }

        }

        protected void Initialize_ComputerOrders(ApplicationDbContext dc)
        {
            //Creating & Adding HardwareItems to database.
            Specifications item1 = new Specifications(0001, "Mid Tower PC Case", "Zalmac Z9 Mid Tower Case with Fan Controller.", 39.99, 25);
            dc.Specifications.Add(item1);

            Specifications item2 = new Specifications(0002, "Intel i5 Processor", "I5 processor provides you with amazing performance, stunning visuals, and better security for deeper protection.", 299.99, 44);
            dc.Specifications.Add(item2);

            Specifications item3 = new Specifications(0003, "AMD FX-6300", "AMD X6 processor gives you better computer experience and better performance for less price.", 129.99, 75);
            dc.Specifications.Add(item3);

            Specifications item4 = new Specifications(004, "G.SKILL X Series RAM", "G.SKILL Ripjaws X Series 16GB (2x8GB) DDR3 2400MHz CL11 Dual Channel Kit (F3-2400C11D-16GXM)", 89.99, 16);
            dc.Specifications.Add(item4);

            //Creating & Adding Customers to database.
            Computer customer = new Computer(1, "Virat Kohli", "Brampton Drive", "L6X1H7");
            customer.orderedDay = DateTime.Now;
            customer.Specs.Add(item1);
            customer.Specs.Add(item2);
            customer.Specs.Add(item4);
            dc.Computers.Add(customer);
            customer = null;

            customer = new Computer(2, "Dwane Smith", "1050 Queen Street", "M2H3C7");
            customer.orderedDay = DateTime.Now;
            customer.Specs.Add(item3);
            dc.Computers.Add(customer);
            customer = null;

            customer = new Computer(3, "Steve Smith", "1 Toronto Street", "M8S4G8");
            customer.orderedDay = DateTime.Now;
            customer.Specs.Add(item1);
            customer.Specs.Add(item3);
            dc.Computers.Add(customer);
            customer = null;
        }

    }
}