﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GP_Lab6.Startup))]
namespace GP_Lab6
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
