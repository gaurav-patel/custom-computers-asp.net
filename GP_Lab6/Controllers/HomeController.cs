﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GP_Lab6.ViewModels;

namespace GP_Lab6.Controllers
{
    public class HomeController : Controller
    {
        private Repo_Computers repo = new Repo_Computers();

        public ActionResult Index()
        {
            return View(repo.getListOfCustomersComputers());
        }

        public ActionResult About()
        {
            ViewBag.Message = "INT422 - Quiz 5";
            var path = System.IO.Path.Combine(Server.MapPath("~/App_Data/"), "q5.html");
            ViewBag.MyData = System.IO.File.ReadAllText(path);
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}