﻿using GP_Lab6.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GP_Lab6.Controllers
{
    public class ComputerController : Controller
    {
        private Repo_Computers repo = new Repo_Computers();
        static GetClientComputer getComputer = new GetClientComputer();

        // GET: Computer
        [Authorize(Roles = "Administrator, Member")]
        public ActionResult Index()
        {
            return View(repo.getListOfCustomersComputers());
        }

        // GET: Computer/Details/5
        [Authorize(Roles = "Administrator, Member")]
        public ActionResult Details(int? id)
        {
            if (id <= 0 || id == null)
            {
                return View("Error");
            }

            var computer = repo.getComputerFull(id);
            if (computer == null)
                return View("Error");
            else
                return View(computer);
        }

        // GET: Computer/Create
        [Authorize(Roles = "Administrator, Member")]
        public ActionResult Create()
        {
            getComputer.specs = repo.createSelectListItems();
            getComputer.Clear();
            return View(getComputer);
        }

        // POST: Computer/Create
        [HttpPost]
        public ActionResult Create(PostComputer newComp)
        {
            if (ModelState.IsValid)
            {
                var comp = repo.createComputer(newComp);
                if (comp == null)
                {
                    return View("Error");
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            else
            {
                //var errorMessage = "Must select at least one computer specification.";
                //if (newComp.specs == null) ModelState.AddModelError("specs", errorMessage);

                getComputer.Name = newComp.Name;
                getComputer.StreetAddress = newComp.StreetAddress;
                getComputer.PostalCode = newComp.PostalCode;

                return View(getComputer);
            }
        }

        // GET: Computer/Edit/5
        [Authorize(Roles = "Administrator, Member")]
        public ActionResult Edit(int? id)
        {
            if (id <= 0 || id == null)
            {
                return View("Error");
            }

            GetClientComputer computer = repo.getComputerforEdit(id);

            if (computer == null)
                return View("Error");
            else
                return View(computer);
        }

        // POST: Computer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, PostComputer edited)
        {
            if (ModelState.IsValid)
            {
                var comp = repo.updateComputer(id, edited);
                if (comp == null)
                {
                    return View("Error");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                var errorMessage = "Must select at least one computer specification.";
                if (edited.specs == null) ModelState.AddModelError("specs", errorMessage);

                getComputer = repo.getComputerforEdit(id);

                getComputer.Name = edited.Name;
                getComputer.StreetAddress = edited.StreetAddress;
                getComputer.PostalCode = edited.PostalCode;

                return View(getComputer);
            }
        }

        // GET: Computer/Delete/5
        [Authorize(Roles="Administrator")]
        public ActionResult Delete(int? id)
        {
            if (id <= 0 || id == null)
            {
                return RedirectToAction("Index");
            }
            ComputerFull comp = repo.getComputerFull(id);

            if (comp == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View(comp);
            }
        }

        // POST: Computer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            bool check = repo.deleteCustomerComputer(id);

            return RedirectToAction("Index");
        }
    }
}
