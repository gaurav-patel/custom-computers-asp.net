﻿using GP_Lab6.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GP_Lab6.Controllers
{
    public class SpecificationController : Controller
    {
        private Repo_Specs repo = new Repo_Specs();

        // GET: Specifications
        [Authorize(Roles = "Administrator, Member")]
        public ActionResult Index()
        {
            return View(repo.getListOfComputerSpecs());
        }

        // GET: Specifications/Details/5
        [Authorize(Roles = "Administrator, Member")]
        public ActionResult Details(int? id)
        {
            if (id <= 0 || id == null)
            {
                return View("Error");
            }

            var spec = repo.getSpecificationFull(id);
            if (spec == null)
                return View("Error");
            else
                return View(spec);
        }

        // GET: Specifications/Create
        [Authorize(Roles = "Administrator")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Specifications/Create
        [HttpPost]
        public ActionResult Create(SpecsFull newSpec)
        {
            if (ModelState.IsValid)
            {
                repo.createSpecification(newSpec);
                return RedirectToAction("Index");
            }

            return View(newSpec);
        }

        // GET: Specifications/Edit/5
        [Authorize(Roles = "Administrator, Member")]
        public ActionResult Edit(int? id)
        {
            if (id <= 0 || id == null)
            {
                return View("Error");
            }

            var edit = repo.getSpecificationFull(id);

            if (edit == null)
                return View("Error");
            else
                return View(edit);
        }

        // POST: Specifications/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, SpecsFull edited)
        {
            if (ModelState.IsValid)
            {
                SpecsFull edit = repo.editSpecification(id, edited);
                if (edit != null) return RedirectToAction("Index");
            }
            return View(edited);
        }

        // GET: Specifications/Delete/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int? id)
        {
            if (id <= 0 || id == null)
            {
                return RedirectToAction("Index");
            }
            SpecsFull spec = repo.getSpecificationFull(id);

            if (spec == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View(spec);
            }
        }

        // POST: Specifications/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            bool check = repo.deleteSpecification(id);

            return RedirectToAction("Index");
        }
    }
}
